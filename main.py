#!/usr/bin/python3

import pygame as pg
from pygame.locals import QUIT
import sys
from collision.poly import Vector
from entities import createBot, createObstacle, createDestination
from systems import (
    DetectorCollisionProcessor,
    BotColorerProcessor,
    BotMoverProcessor,
    RenderProcessor,
    DetectorRenderProcessor,
    ThetaCombinator,
    VelocityComputer,
    BotBehaviorAvoidance,
    BotBehaviorAggregation,
    BotBehaviorDispersion,
    BotBehaviorHoming,
    BotBehaviorConsensus
)
from components import Velocity, Polygon, Color
import esper
import math

v = Vector
FPS = 60
SCREENSIZE = (1000, 800)
screen = pg.display.set_mode(SCREENSIZE)
clock = pg.time.Clock()
world = esper.World()

world.add_processor(DetectorCollisionProcessor())
world.add_processor(BotColorerProcessor())

world.add_processor(BotBehaviorAvoidance())
world.add_processor(BotBehaviorAggregation())
world.add_processor(BotBehaviorDispersion())
world.add_processor(BotBehaviorHoming())
world.add_processor(BotBehaviorConsensus())
world.add_processor(ThetaCombinator())
world.add_processor(VelocityComputer())

world.add_processor(BotMoverProcessor())
world.add_processor(RenderProcessor(screen))
world.add_processor(DetectorRenderProcessor(screen))

# bot1 = createBot(world, v(100, 100))
# bot2 = createBot(world, v(200, 200))
# p0 = createBot(world, v(0, 0))
# p1 = createBot(world, v(500, 500))

rectangle_shape_1 = [v(-100, -5), v(-100, 5), v(100, 5), v(100, -5)]
number = 10
scale = 30
smooth_shape_1 = [
    v(
        math.cos(x * 2 * math.pi / number) * scale,
        math.sin(x * 2 * math.pi / number) * scale,
    )
    for x in range(number)
]

createDestination(world, v(SCREENSIZE[0]/2, 100))

# createObstacle(world, v(250, SCREENSIZE[1] - 350), rectangle_shape_1)
# createObstacle(world, v(100, SCREENSIZE[1] - 200), rectangle_shape_1)
# createObstacle(world, v(400, SCREENSIZE[1] - 200), rectangle_shape_1)
createObstacle(world, v(75, SCREENSIZE[1] - 500), smooth_shape_1)
createObstacle(world, v(250, SCREENSIZE[1] - 500), smooth_shape_1)
createObstacle(world, v(425, SCREENSIZE[1] - 500), smooth_shape_1)
createObstacle(world, v(160, SCREENSIZE[1] - 650), smooth_shape_1)
createObstacle(world, v(330, SCREENSIZE[1] - 650), smooth_shape_1)

createBot(world, v(100, SCREENSIZE[1] - 50))
createBot(world, v(200, SCREENSIZE[1] - 60))
createBot(world, v(300, SCREENSIZE[1] - 70))
createBot(world, v(400, SCREENSIZE[1] - 80))


running = True
while running:
    for event in pg.event.get():
        if event.type == pg.QUIT:
            running = False
        elif event.type == pg.KEYDOWN:
            if event.key == pg.K_ESCAPE:
                running = False

    screen.fill((0, 0, 0))

    # world.add_component(bot1, Velocity(r=0.005))
    # world.add_component(bot2, Velocity(r=-0.01))
    # world.add_component(p0, Velocity(v(0.5, 0.5), 0.01))
    # world.add_component(p1, Velocity(v(-0.5, -0.5), -0.02))

    # A single call to world.process() will update all Processors:
    deltatime = 0
    world.process(deltatime)

    pg.display.flip()
    clock.tick(FPS)

pg.quit()
# sys.exit()


def circ_plt(number, scale):
    test = [
        (
            math.cos(x * 2 * math.pi / number) * scale,
            math.sin(x * 2 * math.pi / number) * scale,
        )
        for x in range(number)
    ]
    x, y = zip(*test)
    plt.scatter(x, y)
    for i in range(len(test)):
        plt.annotate(str(i), (x[i], y[i]))
    plt.show()
