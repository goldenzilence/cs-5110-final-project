from typing import List
from collision.poly import Concave_Poly, Vector
import math

v = Vector

"""
Polygon can be used for position, oritentation, perimiter rendering, and colission detection
"""

default_shape = [v(-15, -15), v(-15, 8), v(-8, 15), v(8, 15), v(15, 8), v(15, -15)]


class Polygon(object):
    def __init__(
        self,
        center: Vector,
        perimiter_points: List[Vector] = default_shape,
        init_rotation: float = 0,
    ) -> None:
        self.body = Concave_Poly(center, perimiter_points, init_rotation)
        return

    # def move(self, delta: Vector) -> None:
    #     self.body.pos += delta
    #     return

    # def rotate(self, delta_radians: float) -> None:
    #     self.body.angle += delta_radians
    #     return
