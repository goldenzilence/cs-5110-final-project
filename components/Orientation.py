"""
When added to an entity with a Polygon it defines how far to move the entity in the next tick
"""
class Orientation(object):
    def __init__(self):
        self.theta_current = 0
        self.theta_target = 0
        self.theta_homing = 0
        self.theta_aggregation = 0
        self.theta_dispersion = 0
        self.theta_avoidance = 0
        self.theta_consensus = 0
        return