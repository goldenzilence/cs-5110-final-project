from collision.poly import Vector

v = Vector

"""
When added to an entity with a Polygon it defines how far to move the entity in the next tick
"""
class Velocity(object):
    def __init__(self, delta_pos:Vector=v(0, 0), r:float=0.0):
        self.delta_position = delta_pos
        self.delta_rotation = r
        return

class VelocityLimits(object):
    def __init__(self):
        self.max_move = 0.6
        self.max_rotate = 0.1
        return