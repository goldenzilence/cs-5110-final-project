from typing import List, Optional, Tuple
from collision.poly import Concave_Poly, Vector
import math

v = Vector

"""
Detector can be used for position, oritentation, perimiter rendering, and colission detection
"""

default_shape = [v(-7.6, 16.6), v(-2, 55), v(-55, 55), v(-55, 2), v(-16.6, 7.6)]


class DetectorComponent(object):
    def __init__(
        self,
        center: Vector,
        perimiter_points: Optional[List[Vector]] = None,
        init_rotation: int = 0,
    ) -> None:
        self.body: Concave_Poly = Concave_Poly(center, default_shape, init_rotation)
        self.didCollide: bool = False
        self.main_color: Tuple[int, int, int] = (255, 255, 255)
        self.collision_color: Tuple[int, int, int] = (255, 0, 0)
        return

    # def move(self, delta: Vector) -> None:
    #     self.body.pos += delta
    #     return

    # def rotate(self, delta_radians: float) -> None:
    #     self.body.angle += delta_radians
    #     return


class LeftDetector(object):
    def __init__(
        self,
        center: Vector,
        perimiter_points: Optional[List[Vector]] = None,
        init_rotation: float = 0,
    ) -> None:
        self.body: Concave_Poly = Concave_Poly(center, default_shape, init_rotation)
        self.detectedObject: bool = False
        return


class RightDetector(object):
    def __init__(
        self,
        center: Vector,
        perimiter_points: Optional[List[Vector]] = None,
        init_rotation: float = -math.pi / 2,
    ) -> None:
        self.body: Concave_Poly = Concave_Poly(center, default_shape, init_rotation)
        self.detectedObject: bool = False
        return
