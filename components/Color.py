from typing import Tuple

"""
Color holds the various types of colors an entity could want to display
"""


class Color(object):
    def __init__(
        self,
        main: Tuple[int, int, int] = (0, 255, 255),
        collision: Tuple[int, int, int] = (255, 0, 0),
        width: int = 3,
    ) -> None:
        self.no_collision_color = main
        self.collision_color = collision
        self.draw_color = main
        self.line_width = width
