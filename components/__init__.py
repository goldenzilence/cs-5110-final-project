"""
ECS (Entity Component System) components
"""

from .Color import Color
from .Detector import *
from .Polygon import Polygon
from .Velocity import Velocity, VelocityLimits
from .Tags import *
from .Orientation import Orientation

__title__ = "components"
__author__ = "Aaron Fine"
__copyright__ = "Copyright 2019 Aaron Fine"
__license__ = "MIT"
__version__ = "1.0.0"
