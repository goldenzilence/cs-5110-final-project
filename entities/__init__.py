"""
ECS (Entity Component System) entities
"""

from .Bot import *

__title__ = "entities"
__author__ = "Aaron Fine"
__copyright__ = "Copyright 2019 Aaron Fine"
__license__ = "MIT"
__version__ = "1.0.0"
