from typing import List
from collision.poly import Concave_Poly, Vector
from components import (
    Polygon,
    Color,
    DetectorComponent,
    LeftDetector,
    RightDetector,
    Bot,
    Obstacle,
    Orientation,
    VelocityLimits,
    Target
)
import math


def createBot(world, center: Vector) -> int:
    return world.create_entity(
        Bot(),
        Polygon(center),
        Color(),
        LeftDetector(center),
        RightDetector(center),
        Orientation(),
        VelocityLimits(),
    )


def createObstacle(world, center: Vector, outline: List[Vector]) -> int:
    return world.create_entity(
        Obstacle(),
        Color((235, 239, 112)),
        Polygon(center=center, perimiter_points=outline),
    )

def createDestination(world, center: Vector) -> int:
    return world.create_entity(
        Target(),
        Color(main=(56, 110, 196), width=1),
        Polygon(center=center, perimiter_points=[Vector(-10, -10), Vector(-10, 10), Vector(10, 10), Vector(10, -10)]),
    )

# class Bot(object):
#     def __init__(self, world, center: Vector) -> None:
#         self.body = Polygon(center)
#         self.left_detector = DetectorComponent(center, 0)
#         self.right_detector = DetectorComponent(center, -math.pi / 2)

#         self.bot = world.create_entity(
#             self.body,
#             Color(),
#             self.left_detector,
#             self.right_detector
#         )

#     def move(self, delta: Vector) -> None:
#         self.body.move(delta)
#         self.left_detector.move(delta)
#         self.right_detector.move(delta)
#         return

#     def rotate(self, angle: float) -> None:
#         self.body.rotate(angle)
#         self.left_detector.rotate(angle)
#         self.right_detector.rotate(angle)
#         return
