from collision.tests import collide
from components import LeftDetector, RightDetector, Orientation
import esper
import math


class BotBehaviorAvoidance(esper.Processor):
    def __init__(self):
        super().__init__()

    def process(self, deltaTime):
        for entity, (left, right, thetas) in self.world.get_components(
            LeftDetector, RightDetector, Orientation
        ):
            if left.detectedObject:
                thetas.theta_avoidance = thetas.theta_current - math.pi / 4
            elif right.detectedObject:
                thetas.theta_avoidance = thetas.theta_current + math.pi / 4

            # Testing
            # thetas.theta_homing = thetas.theta_avoidance
            # thetas.theta_aggregation = thetas.theta_avoidance
            # thetas.theta_dispersion = thetas.theta_avoidance
            # thetas.theta_avoidance = 1.5
            # thetas.theta_consensus = thetas.theta_avoidance

        return
