import esper
import pygame
from components import DetectorComponent, Color, LeftDetector, RightDetector


class DetectorRenderProcessor(esper.Processor):
    def __init__(self, window):
        super().__init__()
        self.window = window

    def process(self, deltaTime):

        # TODO: find a way to iterate over multiple detectors on an entity or create seperate left and right detector components
        for ent, detector in self.world.get_component(DetectorComponent):
            if detector.didCollide:
                pygame.draw.polygon(
                    self.window, detector.collision_color, detector.body.points, 1
                )
            else:
                pygame.draw.polygon(
                    self.window, detector.main_color, detector.body.points, 1
                )

        for ent, detector in self.world.get_component(LeftDetector):
            pygame.draw.polygon(self.window, (255, 255, 255), detector.body.points, 1)

        for ent, detector in self.world.get_component(RightDetector):
            pygame.draw.polygon(self.window, (255, 255, 255), detector.body.points, 1)
        # Flip the framebuffers
        # pygame.display.flip()
