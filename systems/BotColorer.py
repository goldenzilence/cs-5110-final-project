from collision.tests import collide
from components import DetectorComponent, Color, Bot, LeftDetector, RightDetector
import esper


class BotColorerProcessor(esper.Processor):
    def __init__(self):
        super().__init__()

    def process(self, deltaTime):
        for entity, (det, color) in self.world.get_components(DetectorComponent, Color):
            if det.didCollide:
                color.draw_color = color.collision_color
            else:
                color.draw_color = color.no_collision_color

        for entity, bot in self.world.get_component(Bot):
            left = self.world.component_for_entity(entity, LeftDetector)
            right = self.world.component_for_entity(entity, RightDetector)
            color = self.world.component_for_entity(entity, Color)

            if left.detectedObject or right.detectedObject:
                color.draw_color = color.collision_color
            else:
                color.draw_color = color.no_collision_color
        return
