from collision.tests import collide
from components import Polygon, Orientation, Target
import esper
import math
import operator


class BotBehaviorHoming(esper.Processor):
    def __init__(self):
        super().__init__()

    def process(self, deltaTime):
        for entity, (thetas, our_poly) in self.world.get_components(
            Orientation, Polygon
        ):
            our_position = our_poly.body.pos

            # Find position of target
            target_position = None
            for other_entity, (dest, other_poly) in self.world.get_components(
                Target, Polygon
            ):
                target_position = other_poly.body.pos

            # calculate homing behavior
            thetas.theta_homing = math.atan2(
                target_position.y - our_position.y, target_position.x - our_position.x
            )

        return
