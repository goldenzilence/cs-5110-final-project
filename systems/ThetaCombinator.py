from components import Orientation
import esper


class ThetaCombinator(esper.Processor):
    def __init__(self):
        super().__init__()

    def process(self, deltaTime):
        for entity, thetas in self.world.get_component(Orientation):
            # Consensus algo:
            # theta_target = weigting_factor * theta_consensus + (1 - weighting_factor)theta_behavior
            #
            # theta_behavior = weighted sum of behaviors
            theta_behavior =  0.1 * thetas.theta_avoidance
            theta_behavior += 0.2 * thetas.theta_homing
            theta_behavior += 0.4 * thetas.theta_aggregation
            theta_behavior += 0.3 * thetas.theta_dispersion

            weight = .2
            thetas.theta_target = (
                weight * thetas.theta_consensus + (1 - weight) * theta_behavior
            )

        return
