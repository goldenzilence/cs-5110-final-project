from components import Orientation, VelocityLimits, Velocity
from collision.poly import Vector
import esper
import math


class VelocityComputer(esper.Processor):
    def __init__(self):
        super().__init__()

    def process(self, deltaTime):
        for entity, (thetas, limits) in self.world.get_components(
            Orientation, VelocityLimits
        ):
            delta_r = thetas.theta_target - thetas.theta_current
            delta_r -= math.pi / 2

            delta_x = math.cos(thetas.theta_current + math.pi / 2) * limits.max_move
            delta_y = math.sin(thetas.theta_current + math.pi / 2) * limits.max_move
            self.world.add_component(
                entity, Velocity(Vector(delta_x, delta_y), delta_r)
            )
        return
