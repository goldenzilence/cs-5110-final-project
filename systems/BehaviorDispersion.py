from collision.tests import collide
from components import Polygon, Orientation, Bot
import esper
import math
import operator


class BotBehaviorDispersion(esper.Processor):
    def __init__(self):
        super().__init__()

    def process(self, deltaTime):
        for entity, (thetas, our_poly) in self.world.get_components(
            Orientation, Polygon
        ):
            our_position = our_poly.body.pos

            # Find position of closest two connected bots
            positions = []
            for other_entity, (bot, other_poly) in self.world.get_components(
                Bot, Polygon
            ):
                if entity == other_entity:
                    continue
                positions.append(other_poly.body.pos)
            closest = getNeighbors(positions, our_position, 2)

            # find position of the mean of the two nearest connected bots
            x_d = (closest[0][0] + closest[1][0])/2
            y_d = (closest[0][1] + closest[1][1])/2

            # calculate dispersion behavior
            thetas.theta_dispersion = -math.atan2(
                y_d - our_position.y, x_d - our_position.x
            )

        return


# From https://machinelearningmastery.com/tutorial-to-implement-k-nearest-neighbors-in-python-from-scratch/
def euclideanDistance(instance1, instance2, length):
    distance = 0
    for x in range(length):
        distance += pow((instance1[x] - instance2[x]), 2)
    return math.sqrt(distance)


def getNeighbors(trainingSet, testInstance, k):
    distances = []
    length = len(testInstance) - 1
    for x in range(len(trainingSet)):
        dist = euclideanDistance(testInstance, trainingSet[x], length)
        distances.append((trainingSet[x], dist))
    distances.sort(key=operator.itemgetter(1))
    neighbors = []
    for x in range(k):
        neighbors.append(distances[x][0])
    return neighbors
