from collision.tests import collide
from components import Polygon, Orientation, Bot
import esper
import math
import operator


class BotBehaviorConsensus(esper.Processor):
    def __init__(self):
        super().__init__()

    def process(self, deltaTime):
        angle_sum = 0
        count = 0

        for entity, (bot, poly) in self.world.get_components(Bot, Polygon):
            angle_sum += poly.body.angle
            count += 1

        k = 1 / count
        consensus_angle = angle_sum * k

        for entity, thetas in self.world.get_component(Orientation):
            thetas.theta_consensus = consensus_angle

        return
