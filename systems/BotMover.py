from collision.tests import collide
from collision.poly import Concave_Poly
from components import (
    Polygon,
    Velocity,
    DetectorComponent,
    LeftDetector,
    RightDetector,
    Orientation,
)
from entities import Bot
import esper
import copy


class BotMoverProcessor(esper.Processor):
    def __init__(self):
        super().__init__()

    def process(self, deltaTime):
        for entity, (poly, deltaMove, left, right) in self.world.get_components(
            Polygon, Velocity, LeftDetector, RightDetector
        ):
            # Don't allow collisions
            test_body = copy.copy(poly.body)
            test_body.pos += deltaMove.delta_position
            test_body.angle += deltaMove.delta_rotation
            for other, other_poly in self.world.get_component(Polygon):
                if entity == other:
                    continue

                if collide(test_body, other_poly.body):
                    deltaMove.delta_position = 0

            poly.body.pos += deltaMove.delta_position
            poly.body.angle += deltaMove.delta_rotation

            left.body.pos += deltaMove.delta_position
            left.body.angle += deltaMove.delta_rotation

            right.body.pos += deltaMove.delta_position
            right.body.angle += deltaMove.delta_rotation
            self.world.remove_component(entity, Velocity)

            for thetas in self.world.try_component(entity, Orientation):
                thetas.theta_current = poly.body.angle

        # for entity, (poly, deltaMove) in self.world.get_components(Polygon, Velocity):
        #     poly.body.pos += deltaMove.delta_position
        #     poly.body.angle += deltaMove.delta_rotation
        #     self.world.remove_component(entity, Velocity)
        return
