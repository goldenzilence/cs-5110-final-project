import esper
import pygame
from components import Polygon, Color


class RenderProcessor(esper.Processor):
    def __init__(self, window, clear_color=(0, 0, 0)):
        super().__init__()
        self.window = window
        self.clear_color = clear_color

    def process(self, deltaTime):
        # Clear the window:
        self.window.fill(self.clear_color)
        # This will iterate over every Entity that has this Component, and blit it:
        for ent, (poly, color) in self.world.get_components(Polygon, Color):
            pygame.draw.polygon(
                self.window, color.draw_color, poly.body.points, color.line_width
            )
        # Flip the framebuffers
        # pygame.display.flip()
