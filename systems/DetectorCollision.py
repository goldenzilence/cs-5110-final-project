from collision.tests import collide
from components import DetectorComponent, Polygon, LeftDetector, RightDetector
import esper


class DetectorCollisionProcessor(esper.Processor):
    def __init__(self):
        super().__init__()

    def process(self, deltaTime):
        for ent, detector in self.world.get_component(DetectorComponent):
            didDetectPolygon = False
            for other, poly in self.world.get_component(Polygon):
                if collide(detector.body, poly.body):
                    didDetectPolygon = True
                    break
            detector.didCollide = didDetectPolygon

        for ent, detector in self.world.get_component(LeftDetector):
            didDetectPolygon = False
            for other, poly in self.world.get_component(Polygon):
                if collide(detector.body, poly.body):
                    didDetectPolygon = True
                    break
            detector.detectedObject = didDetectPolygon

        for ent, detector in self.world.get_component(RightDetector):
            didDetectPolygon = False
            for other, poly in self.world.get_component(Polygon):
                if collide(detector.body, poly.body):
                    didDetectPolygon = True
                    break
            detector.detectedObject = didDetectPolygon
        return
