from collision.tests import collide
from components import Polygon, Orientation, Bot
import esper
import math


class BotBehaviorAggregation(esper.Processor):
    def __init__(self):
        super().__init__()

    def process(self, deltaTime):
        for entity, (thetas, our_poly) in self.world.get_components(
            Orientation, Polygon
        ):
            our_position = our_poly.body.pos
            max_distance = 0
            max_pos = None

            # Find position of farthest connected bot
            for other_entity, (bot, other_poly) in self.world.get_components(
                Bot, Polygon
            ):
                if entity == other_entity:
                    continue
                other_position = other_poly.body.pos
                # NOTE: We don't need to take the square root here because we just need to 
                # find the position corresponding to the largest distance, not the distance itself
                distance = (our_position.x - other_position.x) ** 2 + (
                    our_position.y - other_position.y
                ) ** 2
                if distance > max_distance:
                    max_distance = distance
                    max_pos = other_position

            thetas.theta_aggregation = math.atan2(
                max_pos.y - our_position.y, max_pos.x - our_position.x
            )

        return
