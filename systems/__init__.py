"""
ECS (Entity Component System) systems that don't need to touch PyGame
"""

from .DetectorCollision import DetectorCollisionProcessor
from .BotColorer import BotColorerProcessor
from .BotMover import BotMoverProcessor
from .PolyRenderer import RenderProcessor
from .DetectorRenderer import DetectorRenderProcessor
from .BehaviorAvoid import BotBehaviorAvoidance
from .BehaviorAggregate import BotBehaviorAggregation
from .BehaviorDispersion import BotBehaviorDispersion
from .ThetaCombinator import ThetaCombinator
from .VelocityComputer import VelocityComputer
from .BehaviorHoming import BotBehaviorHoming
from .BehaviorConsensus import BotBehaviorConsensus

__title__ = "systems"
__author__ = "Aaron Fine"
__copyright__ = "Copyright 2019 Aaron Fine"
__license__ = "MIT"
__version__ = "1.0.0"
